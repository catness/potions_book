package org.catness.potionsbook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
// import android.widget.Toast
//import android.util.Log

class MainActivity : AppCompatActivity() {
    lateinit var next: ImageView
    lateinit var previous: ImageView
    lateinit var name: TextView
    lateinit var potion_image: ImageView
    lateinit var description: TextView
    lateinit var potions: List<Potion>
    var currentPotion: Int = 0
    var numPotions: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //to change title of activity
        val actionBar = supportActionBar
        actionBar!!.title = getString(R.string.full_title)

        next = findViewById(R.id.next)
        previous = findViewById(R.id.previous)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        potion_image = findViewById(R.id.potion_image)
        next.setOnClickListener { showNext() }
        previous.setOnClickListener { showPrevious() }

        potions = potionsInit()
        numPotions = potions.size
        showPotion()
        //Log.d("MAIN",potions[0].name + " : " + potions[0].description)
    }

    private fun showPotion() {
        name.setText(potions[currentPotion].name)
        description.setText(potions[currentPotion].description)
        potion_image.setImageResource(potions[currentPotion].image)
    }

    private fun showNext() {
        if (++currentPotion >= numPotions) currentPotion = 0
        showPotion()
    }

    private fun showPrevious() {
        if (--currentPotion < 0) currentPotion = numPotions - 1
        showPotion()
    }
}