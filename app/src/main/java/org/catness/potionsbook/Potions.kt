package org.catness.potionsbook

data class Potion(val name : String, val description : String, val image : Int)


fun potionsInit() : List<Potion> {
    return listOf (
        Potion(
            "Chaos Potion",
"""
To brew a chaos potion, gather the following ingredients and follow the recipe well, but feel free to use close substitutes if required.
- 4 teaspoons of Autumn Culantro
- 2 pinches of Stinky Galangal
- 4 tablespoons of Winter Grass
- 4 bits of Thunder Sage
- 1 sprinkle of Flux Clove

Break the Autumn Culantro with your hands and put the result in a container. Add some purified water to turn it into what's now basically dirty water, or tea, then add the Stinky Galangal, first one tiny amount, then everything else.
Fill a pan with purified water, add the mixture and bring it to a boil, then freeze it, then bring it to a simmer. Let it cool down to the body temperature of a reptile before adding the Winter Grass and Thunder Sage, a half at a time, alternating between the two.
Quickly bring everything up to a boil, mix in the Flux Clove and let everything cool down until the mixture is thick and gloopy.

Whisk the mixture heavily to aerate it. Stop when it's frothy. Your potion is ready to be used. A few sips is all that's needed for the potion to work. A few gulps will work too, but a few slurps will be too little and not very classy.

Recipe by Tim the Enchanter.
""",
            R.drawable.potion1),
        Potion(
            "Hidden Talent Potion",
"""
To brew a hidden talent potion, gather the following ingredients and follow the steps with absolute precision.
- 6 bits of Candy Salt
- 2 batches of Devil Blossom
- 4 bits of King's Saffron
- 4 sprinkles of Thorn Pepper
- 5 bits of Arctic Basil

Mash the Candy Salt with whatever you can find and put it in a container. Add a dash of preboiled water to turn it into a cream-like paste, then add the Devil Blossom in tiny amounts at a time.
Fill a pan with rainwater, add the mixture and turn up the heat, then turn on the stove and bring the mixture to a boil. Let it boil until you can smell it's burned before adding the King's Saffron and Thorn Pepper, in that order.
Bring everything back to a boil, turn of the heat, mix in the Arctic Basil and let everything cool down and rest, you should probably do the same.

Stir the mixture a final time, if it sticks you know it's right. Your potion is ready to be used. A lot is needed for the potion to work, like 'a lot' a lot. Like you need to practically bathe in it 'a lot'.

Recipe by Dr. Bunsen Honeydew, Tested on Beaker.
""",
            R.drawable.potion2),
        Potion(
            "Vigor Potion",
"""
To brew a vigor potion, gather the following ingredients and stick to the recipe below as much as you can.
- 2 sprinkles of Arctic Moss
- 2 pinches of Rain Saffron
- 4 teaspoons of Love Quassia
- 2 dashes of Mountain Tarragon
- 3 pinches of Fade Cinnamon

Squeeze the Arctic Moss with your hands, or make your friend do it for you, and put it to a cup. Add a sprinkle of liquid ice to turn it into a messy mix, then add the Rain Saffron in small amounts at a time.
Fill a pan with liquified steam, add the mixture and let it steep while you do something fun, then come back and bring it to a boil. Let it rest overnight before adding the Love Quassia and Mountain Tarragon, add them all at once, but do it very gently.
Slowly, very slowly bring the mixture up to a simmer, mix in the Fade Cinnamon and let everything cool down. If you touch the liquid and it burns you, it hasn't cooled down enough.

Whisk the mixture heavily to aerate it. Stop when it's frothy. Your potion is ready to be used. A few sips is all that's needed for the potion to work. A few gulps will work too, but a few slurps will be too little and not very classy.

Recipe by Victor Frankenstein.
""",
            R.drawable.potion3),

        Potion(
            "Deep Sleep Potion",                
"""To brew a deep sleep potion, gather the following ingredients and follow the steps below, but feel free to add your own twist.
- 2 teaspoons of Mercy Root
- 6 bits of Hazel Bay Leaf
- 4 teaspoons of Abyss Cudweed
- 4 sprinkles of Sea Lovage
- 2 cups of Cavern Mallow

Mash the Mercy Root in a cup, making sure you don't break the cup as it's probably a delicate cup. Add a sprinkle of liquid ice to turn it into a thick emulsion, then add the Hazel Bay Leaf in tiny amounts at a time.
Fill a pan with salty sea water, as in the sea water has to be upset or agitated, add the mixture and cool it down with how cool you are, then bring the mixture to a boil. Let it rest overnight before adding the Abyss Cudweed and Sea Lovage, just throw it all in there, show them who's boss.
Rapidly heat the mixture until it's hot, mix in the Cavern Mallow and let everything cool down and rest for an hour.

Stir everything one last time to make sure what you made is in fact a potion. Your potion is ready to be used. A lot is needed for the potion to work. About a bucket will do. If the potion is meant to kill or incapacitate something, you might as well throw the bucket itself.

Recipe by Morrigan.
""",
            R.drawable.potion4),
        Potion(
            "Free Will Potion",
"""To brew a free will potion, gather the following ingredients and stick to the recipe below as much as you can.
- 1 batch of Queen's Savory
- 5 tablespoons of Knettle Moss
- 1 pinch of Shimmer Saffron
- 1 tablespoon of Bruise Clove
- 3 dashes of Hybernation Tea Leaf

Squash the Queen's Savory and, while doing so, let go of all your stress. Put the result in a container. Add a sprinkle of liquid ice to turn it into what's now basically dirty water, or tea, then add the Knettle Moss in tiny amounts at a time.
Fill a pan with water from the dead sea, if it's too far away, get normal sea water and kill it, add the mixture and gently, very gently bring it to a simmer. Let it steam for a short while before adding the Shimmer Saffron and Bruise Clove, in that order.
Quickly bring everything up to a boil, mix in the Hybernation Tea Leaf and let everything rest while you take a power nap, maybe two since you've worked hard.

Whisk the mixture gently to break up any potential clumps. Your potion is ready to be used. A lot is needed for the potion to work. About a bucket will do. If the potion is meant to kill or incapacitate something, you might as well throw the bucket itself.

Recipe by Wilter Whate.
""",
            R.drawable.potion5),
        Potion(
            "Foresight Potion",
"""
To brew a foresight potion, gather the following ingredients and follow the steps with absolute precision.
- 3 heaps of White Spearmint
- 3 tablespoons of Ivory Leaf
- 2 bits of Ice Bay Leaf
- 1 tablespoon of Silk Sumac
- 5 tablespoons of Assassin Pepper

Break the White Spearmint with your hands, or make your friend do it for you, and put it to a cup. Add a little bit of water to turn it into a thick paste, then add the Ivory Leaf, one half at a time until you've added three parts.
Fill a pan with water, any water, add the mixture and let it rest for a while. Wake up the mixture and bring it to a simmer. Let it steam for a short while before adding the Ice Bay Leaf and Silk Sumac, add them all at once, but do it very gently.
Bring everything back to a boil, turn of the heat, mix in the Assassin Pepper and let everything cool down until the mixture is thick and gloopy.

Whisk the mixture gently to break up any potential clumps. Your potion is ready to be used. A lot is needed for the potion to be effective. It's not very potent. We should probably have used different ingredients.

Recipe by Nemo.
""",
            R.drawable.potion6),
         Potion(
            "Arcane Protection Potion",
"""
To brew an arcane protection potion, gather the following ingredients and follow the recipe with exact precision or risk an uncertain outcome.
- 2 heaps of Fire Petal
- 5 teaspoons of Hate Mustard
- 3 pinches of Sour Barberry
- 4 teaspoons of Tiger Basil
- 2 cups of Queen's Dill

Beat the Fire Petal with a pestle and mortar. Add some clean water to turn it into a cream-like paste, then add the Hate Mustard, a quarter first, then a half and then a third of it.
Fill a pan with unsalted sea water, add the mixture and let it sit there for a while, then bring it to a boil. Let it reduce by half before adding the Sour Barberry and Tiger Basil, just throw it all in there, show them who's boss.
Bring everything back to a boil, turn of the heat, mix in the Queen's Dill and let everything cool down and rest for an hour.

Whisk everything with a knife until you realize you look like a fool. Your potion is ready to be used. A good gulp is needed for potion to be effective, but it tastes awful so maybe mix it with some honey.

Recipe by Dr. Bunsen Honeydew, Tested on Beaker.
""",
            R.drawable.potion7)
        )
}